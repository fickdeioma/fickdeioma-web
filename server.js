const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3000;
const path = require('path');

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));


http.listen(port, function(){
  console.log('listening on *:' + port);
});


//Handle the Huhus with Username

io.on('connection', function(socket){
  console.log('made socket connection' + socket.id);

  socket.on('huhu', function(data){
    socket.broadcast.emit('huhu', data);
  });

});